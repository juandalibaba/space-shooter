import config
from random import randrange, random
from math import sin, cos, radians
from arcade import Sprite, SpriteList,draw_points, color

class Meteor(Sprite):

    def __init__(self):
        x = randrange(config.SCREEN_WIDTH)
        y = randrange(config.SCREEN_HEIGHT)
        super().__init__(filename="sprites/meteorGrey_small1.png", center_x=x, center_y=y, scale=1)

        self.change_angle = 3
        self.change_y = -0.4 - 5 * random()


    def update(self):
        super(Meteor, self).update()

        if self.top < 0:
            self.bottom = config.SCREEN_HEIGHT

class MeteorField(SpriteList):

    def __init__(self):
        super().__init__()
        for i in range(10):
            star = Meteor()
            self.append(star)


class StarField():

    def __init__(self):
        self.points = []
        for i in range(20):
            x = randrange(config.SCREEN_WIDTH)
            y = randrange(config.SCREEN_HEIGHT)
            self.points.append((x,y))

    def update(self):
        points = []
        for point in self.points:
            y = point[1] - 2
            if y < 0:
                y = config.SCREEN_HEIGHT
            points.append((point[0],y))
        self.points = points

    def draw(self):
        draw_points(self.points, color.WHITE, 2)



