import config
from random import uniform
from arcade import (Window,
                    View,
                    SpriteList,
                    set_background_color,
                    start_render,
                    draw_text,
                    check_for_collision_with_list,
                    color,
                    key,
                    set_viewport)

from player import Player
from level_manager import LevelManager
from backgrounds import MeteorField, StarField

BACKGROUND_COLOR = color.BLACK

class InstructionView(View):
    def on_show(self):
        set_background_color(BACKGROUND_COLOR)
        set_viewport(0, config.SCREEN_WIDTH-1, 0, config.SCREEN_HEIGHT - 1)

    def on_draw(self):
        """ Draw this view """
        start_render()
        draw_text("Instructions Screen", config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2,
                         color.WHITE, font_size=50, anchor_x="center")
        draw_text("Click to advance", config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2 - 75,
                         color.WHITE, font_size=20, anchor_x="center")

    def on_mouse_press(self, x: float, y: float, button: int, modifiers: int):
        game_view = GameView()
        game_view.setup()
        self.window.show_view(game_view)


class GameOverView(View):
    def on_show(self):
        set_background_color(BACKGROUND_COLOR)
        set_viewport(0, config.SCREEN_WIDTH - 1, 0, config.SCREEN_HEIGHT - 1)

    def on_draw(self):
        """ Draw this view """
        start_render()
        draw_text("Game Over", config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2,
                  color.WHITE, font_size=50, anchor_x="center")
        draw_text("Click to play again", config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2 - 75,
                  color.WHITE, font_size=20, anchor_x="center")

    def on_mouse_press(self, x: float, y: float, button: int, modifiers: int):
        view = GameView()
        view.setup()
        self.window.show_view(view)


class GameView(View):

    def __init__(self):
        super().__init__()
        set_background_color(BACKGROUND_COLOR)

        self.player_list = None
        self.enemy_list = None
        self.player_laser_list = None
        self.player_laser_list = None
        self.enemies_laser_list = None
        self.background = None

        self.level_manager = LevelManager()

        self.lives = config.LIVES

        self.left_pressed = False
        self.right_pressed = False
        self.space_pressed = False

    def setup(self):
        self.player_list = SpriteList()
        self.enemy_list = SpriteList()
        self.player_laser_list = SpriteList()
        self.enemies_laser_list = SpriteList()
        self.background = StarField()

        self.player_list.append(Player())
        self.player = self.player_list[0]
        self.enemy_list = self.level_manager.create_enemies()


    def update_staff_player(self):
        enemies_hit_list = check_for_collision_with_list(self.player, self.enemy_list)
        enemies_laser_hit_list = check_for_collision_with_list(self.player, self.enemies_laser_list)
        if len(enemies_hit_list) > 0 or len(enemies_laser_hit_list) > 0:
            self.player.die()
            self.setup()
            self.lives -= 1
            if self.lives == 0:
                self.window.show_view(GameOverView())

    def on_draw(self):
        start_render()

        draw_text(f"Score: {self.level_manager.score}",10, 20, color.WHITE)
        draw_text(f"Lives: {self.lives}", 80, 20, color.WHITE)

        self.background.draw()
        self.player_list.draw()
        self.enemy_list.draw()
        self.player_laser_list.draw()
        self.enemies_laser_list.draw()

    def on_update(self, delta_time: float):
        self.background.update()
        self.player_list.update()
        self.player_laser_list.update()
        self.update_staff_player()
        self.level_manager.update_enemies(self.enemy_list, self.enemies_laser_list, self.player_laser_list)

        if self.left_pressed:
            self.player_list[0].move_left()
        if self.right_pressed:
            self.player_list[0].move_right()
        if not self.left_pressed and not self.right_pressed:
            self.player_list[0].stop()

    def on_key_press(self, symbol: int, modifiers: int):
        if symbol == key.LEFT:
            self.left_pressed = True
        elif symbol == key.RIGHT:
            self.right_pressed = True
        elif symbol == key.SPACE:
            laser = self.player_list[0].shot()
            self.player_laser_list.append(laser)
            self.space_pressed = True


    def on_key_release(self, symbol: int, modifiers: int):
        if symbol == key.LEFT:
            self.left_pressed = False
        elif symbol == key.RIGHT:
            self.right_pressed = False
        elif symbol == key.SPACE:
            self.space_pressed = False