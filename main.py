import  config
from arcade import Window, run
from game_view import InstructionView, GameView

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    window = Window(config.SCREEN_WIDTH, config.SCREEN_HEIGHT, "Juegasoooo")
    if config.DEBUG:
        start_view = GameView()
        start_view.setup()
    else:
        start_view = InstructionView()
    window.show_view(start_view)
    run()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
