import math

import config
from arcade import Sprite, SpriteList, play_sound
from sounds import laser_sound, laser_large_sound, laser_follower_sound

class Laser(Sprite):
    def __init__(self, **kwargs):
        super(Laser, self).__init__(filename="sprites/laserGreen11.png",scale=config.LASER_SCALE, **kwargs)
        play_sound(laser_sound)

    def update(self):
        super(Laser, self).update()
        self.change_y = config.LASER_SPEED

        if self.top > config.SCREEN_HEIGHT:
            self.remove_from_sprite_lists()


class EnemyLaser(Sprite):
    def __init__(self, **kwargs):
        super().__init__(filename="sprites/laserRed02.png",scale=config.LASER_SCALE, **kwargs)
        play_sound(laser_large_sound)

    def update(self):
        super().update()
        self.change_y = -config.ENEMY_LASER_SPEED

        if self.bottom < 0:
            self.remove_from_sprite_lists()


class FollowerLaser(Sprite):
    def __init__(self, player, **kwargs):
        super().__init__(filename="sprites/laserRed08.png", scale=config.LASER_SCALE, **kwargs)
        play_sound(laser_follower_sound)

        delta_x = player.center_x - self.center_x
        delta_y = player.center_y - self.center_y
        module = math.sqrt(delta_x ** 2 + delta_y ** 2)

        self.change_x = 10 * delta_x / module
        self.change_y = 10 * delta_y / module

class AngleLaser(Sprite):
    @staticmethod
    def createAngleLaserList(center_x=0, center_y=0):
        laser_list = SpriteList()
        for i in range(1, 5):
            laser_list.append(AngleLaser(i*90/5, center_x=center_x, center_y=center_y))
            laser_list.append(AngleLaser(-i*90/5, center_x=center_x, center_y=center_y))

        return laser_list


    def __init__(self, angle, **kwargs):
        super().__init__(filename="sprites/laserBlue03.png", scale=config.LASER_SCALE, **kwargs)

        rad = math.radians(angle)
        self.change_x = 5 * math.sin(rad)
        self.change_y = - 5 * math.cos(rad)
