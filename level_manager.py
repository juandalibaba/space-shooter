import config
from math import floor
from random import uniform
from arcade import SpriteList, check_for_collision_with_list
from enemies import BlackEnemy, BigEnemy
from bullets import EnemyLaser


def create_line_of_enemies(EnemyType):
    """
    Creates a horizontal line of EnemyType
    :param self:
    :param EnemyType: A enemy type (a class)
    :return: le: A SpriteList of enemies in line
    """
    be = EnemyType()
    we = be.width
    he = be.height
    sw = config.SCREEN_WIDTH
    max_number_of_enemies = floor(sw/we)
    number_of_enemies = floor(0.50 * max_number_of_enemies)
    delta = int(sw/number_of_enemies)
    le = SpriteList()
    for i in range(1, number_of_enemies):
        be = EnemyType(center_x=i*delta, center_y=config.SCREEN_HEIGHT -  he)
        le.append(be)

    return le

class LevelManager():
    def __init__(self):
        self.score = 0
        self.time = 0

    def create_enemies(self):
        return create_line_of_enemies(BlackEnemy)

    def update_enemies(self, enemy_list, enemies_laser_list, player_laser_list):
        self.time += 1
        enemy_list.update()
        if self.time < 300:
            self.level0(enemy_list, enemies_laser_list, player_laser_list)
        elif 300 < self.time < 600:
            self.level1(enemy_list, enemies_laser_list, player_laser_list)
        else:
            self.level2(enemy_list, enemies_laser_list, player_laser_list)

        #self.level1(enemy_list, enemies_laser_list, player_laser_list)
        enemies_laser_list.update()


    def check_collisions(self, enemy, player_laser_list):
        collision_laser_list = check_for_collision_with_list(enemy, player_laser_list)
        if len(collision_laser_list) > 0:
            collision_laser_list[0].remove_from_sprite_lists()
            self.score += 1
            enemy.die()


    def level0(self, enemy_list, enemies_laser_list, player_laser_list):

        for enemy in enemy_list:
            if uniform(0, 1000) < 10:
                laser = EnemyLaser(center_x=enemy.center_x, center_y=enemy.center_y)
                enemies_laser_list.append(laser)

            self.check_collisions(enemy, player_laser_list)



    def level1(self, enemy_list, enemies_laser_list, player_laser_list):
        if self.time % 240 == 0:
            l = create_line_of_enemies(BlackEnemy)
            enemy_list.extend(l)

        self.level0(enemy_list, enemies_laser_list, player_laser_list)

    def level2(self, enemy_list, enemies_laser_list, player_laser_list):
        if self.time % 160 == 0:
            l = create_line_of_enemies(BlackEnemy)
            enemy_list.extend(l)

        self.level0(enemy_list, enemies_laser_list, player_laser_list)