from arcade import load_sound

laser_sound = load_sound("sounds/sfx_laser1.ogg")
laser_large_sound = load_sound("sounds/laserLarge_001.ogg")
lose_sound = load_sound("sounds/sfx_lose.ogg")
explosion_sound = load_sound("sounds/explosionCrunch_000.ogg")
laser_follower_sound = load_sound("sounds/forceField_001.ogg")