import config
from arcade import Sprite, play_sound
from bullets import Laser
from sounds import lose_sound

class Player(Sprite):

    def __init__(self):
        super().__init__(filename="sprites/playerShip1_blue.png", scale=config.PLAYER_SCALE)
        self.center_x = 100
        self.center_y = 100

    def move_left(self):
        self.change_x = -config.PLAYER_SPEED
        if self.left < 0:
            self.right = config.SCREEN_WIDTH

    def move_right(self):
        self.change_x = config.PLAYER_SPEED
        if self.right > config.SCREEN_WIDTH:
            self.left = 0

    def stop(self):
        self.change_x = 0

    def shot(self):
        laser = Laser(center_x=self.center_x,
                      center_y=self.center_y)
        return laser

    def die(self):
        play_sound(lose_sound)