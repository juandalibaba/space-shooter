# CLASSICAL SPACE SHOOTER VIDEOGAME

# Installation

Clone the repo:

```# git clone https://gitlab.com/juandalibaba/space-shooter```

Create a virtualenv:

```
# cd space-shooter
# python -m venv venv
```

Activate the virtualenv and install requirements:

```
# . venv/bin/activate
(venv)# pip install -r requirements.txt
```

Run videogame:

```
(venv)# python main.py
```



