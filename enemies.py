import config
from random import randrange
from math import floor
from arcade import (Sprite,
                    SpriteList,
                    play_sound)
from sounds import explosion_sound
from bullets import EnemyLaser

class Enemy(Sprite):

    def die(self):
        play_sound(explosion_sound)
        self.remove_from_sprite_lists()

    def update(self):
        super(Enemy, self).update()
        if self.top < 0:
            self.bottom = config.SCREEN_HEIGHT



class BlackEnemy(Enemy):

    def __init__(self, **kwargs):
        super().__init__(filename="sprites/enemyBlack1.png",
                         scale=config.ENEMY_SCALE,
                         **kwargs)

        self.change_x = 1
        self.change_y = -1

    def update(self):
        super(BlackEnemy, self).update()
        if self.change_x == 1 and self.right > config.SCREEN_WIDTH:
            self.change_x = -1
        if self.change_x == -1 and self.left < 0:
            self.change_x = 1



class BigEnemy(Enemy):

    def __init__(self, **kwargs):
        super().__init__(filename="sprites/enemyRed1.png", scale=config.ENEMY_SCALE, **kwargs)
        self.change_y = -1




